import request from '@/utils/httpRequest'


const BASE_URL = "/small-coupon-service"

const CPUPON_URL = BASE_URL + "/web/v1.0/coupon/coupon"
const COUPON_HISTORY_URL = BASE_URL + "/web/v1.0/coupon/coupon/history"
const COUPON_HOME_SUBJECT_URL = BASE_URL + "/web/v1.0/coupon/home/subject"
const SECKILL_PROMOTION_URL = BASE_URL + "/web/v1.0/coupon/seckill/promotion"
const SPU_BOUNDS_URL = BASE_URL + "/web/v1.0/coupon/spu/bounds"
const SKU_FULL_URL = BASE_URL + "/web/v1.0/coupon/sku/full/reduction"
const MEMBER_PRICE_URL = BASE_URL + "/web/v1.0/coupon/member/price"
const SECKILL_SESSION_URL = BASE_URL + "/web/v1.0/coupon/seckill/session"
const SECKILL_SKU_RELATION_URL = BASE_URL + "/web/v1.0/coupon/seckill/sku/relation"



// 优惠券请求列表
export function pageList(data) {
    return request({
        url: CPUPON_URL + "/page/list",
        method: 'post',
        data: data
    })
}


// 优惠券删除
export function remove(data) {
    return request({
        url: CPUPON_URL + "/delete",
        method: 'post',
        data: data
    })
}

// 优惠券删除
export function addOrUpdate(data) {
    return request({
        url: CPUPON_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data
    })
}


// 优惠券领取列表方法
export function pageCouponHistoryList(data) {
    return request({
        url: COUPON_HISTORY_URL + "/page/list",
        method: 'post',
        data: data,
    });
}

// 优惠券领取删除方法
export function couponHistoryRemove(data) {
    return request({
        url: COUPON_HISTORY_URL + "/delete",
        method: 'post',
        data: data,
    });
}





// 首页专题表列表方法
export function pageHomeSubjectList(data) {
    return request({
        url: COUPON_HOME_SUBJECT_URL + "/page/list",
        method: 'post',
        data: data,
    });
}

// 首页专题表保存和这修改方法
export function saveOrUpdateHomeSubject(data) {
    return request({
        url: COUPON_HOME_SUBJECT_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}

// 首页专题表保存和这修改方法
export function removeHomeSubject(data) {
    return request({
        url: COUPON_HOME_SUBJECT_URL + "/delete",
        method: 'post',
        data: data,
    });
}


// 首页专题表显示方法
export function enableHomeSubject(data) {
    return request({
        url: COUPON_HOME_SUBJECT_URL + "/enable",
        method: 'post',
        data: data,
    });
}

// 首页专题表不显示方法
export function disableHomeSubject(data) {
    return request({
        url: COUPON_HOME_SUBJECT_URL + "/disable",
        method: 'post',
        data: data,
    });
}






// 秒杀活动列表方法
export function pageSeckillPromotionList(data) {
    return request({
        url: SECKILL_PROMOTION_URL + "/page/list",
        method: 'post',
        data: data,
    });
}

// 秒杀活动保存和修改列表方法
export function saveOrUpdateSeckillPromotion(data) {
    return request({
        url: SECKILL_PROMOTION_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}



// 首页专题表保存和这修改方法
export function removeSeckillPromotion(data) {
    return request({
        url: SECKILL_PROMOTION_URL + "/delete",
        method: 'post',
        data: data,
    });
}


// 首页专题表显示方法
export function enableSeckillPromotion(data) {
    return request({
        url: SECKILL_PROMOTION_URL + "/enable",
        method: 'post',
        data: data,
    });
}

// 首页专题表不显示方法
export function disableSeckillPromotion(data) {
    return request({
        url: SECKILL_PROMOTION_URL + "/disable",
        method: 'post',
        data: data,
    });
}



// 商品spu积分列表方法
export function pageSpuBoundsList(data) {
    return request({
        url: SPU_BOUNDS_URL + "/page/list",
        method: 'post',
        data: data,
    });
}



// 商品spu积分保存或者修改方法
export function saveOrUpdateSpuBounds(data) {
    return request({
        url: SPU_BOUNDS_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}

// 商品spu积分删除方法
export function removeSpuBounds(data) {
    return request({
        url: SPU_BOUNDS_URL + "/delete",
        method: 'post',
        data: data,
    });
}



// 满减折扣列表方法
export function pageSkuFullList(data) {
    return request({
        url: SKU_FULL_URL + "/page/list",
        method: 'post',
        data: data,
    });
}



// 满减折扣保存或者修改方法
export function saveOrUpdateSkuFull(data) {
    return request({
        url: SKU_FULL_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}

// 满减折扣删除方法
export function removeSkuFull(data) {
    return request({
        url: SKU_FULL_URL + "/delete",
        method: 'post',
        data: data,
    });
}



// 会员价格列表方法
export function pageMemberPriceList(data) {
    return request({
        url: MEMBER_PRICE_URL + "/page/list",
        method: 'post',
        data: data,
    });
}



// 会员价格保存或者修改方法
export function saveOrUpdateMemberPrice(data) {
    return request({
        url: MEMBER_PRICE_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}

// 会员价格扣删除方法
export function removeMemberPricel(data) {
    return request({
        url: MEMBER_PRICE_URL + "/delete",
        method: 'post',
        data: data,
    });
}




// 每日秒杀列表方法
export function pageSeckillSessionList(data) {
    return request({
        url: SECKILL_SESSION_URL + "/page/list",
        method: 'post',
        data: data,
    });
}



// 每日秒杀保存或者修改方法
export function saveOrUpdateSeckillSession(data) {
    return request({
        url: SECKILL_SESSION_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}

// 每日秒杀扣删除方法
export function removeSeckillSession(data) {
    return request({
        url: SECKILL_SESSION_URL + "/delete",
        method: 'post',
        data: data,
    });
}


// 每日秒杀启用方法
export function enableSeckillSession(data) {
    return request({
        url: SECKILL_SESSION_URL + "/enable",
        method: 'post',
        data: data,
    });
}


// 每日秒杀禁用方法
export function disableSeckillSession(data) {
    return request({
        url: SECKILL_SESSION_URL + "/disable",
        method: 'post',
        data: data,
    });
}




// 秒杀活动商品关联列表方法
export function pageSeckillSkuRelationList(data) {
    return request({
        url: SECKILL_SKU_RELATION_URL + "/page/list",
        method: 'post',
        data: data,
    });
}



// 秒杀活动商品关联保存或者修改方法
export function saveOrUpdateSeckillSkuRelation(data) {
    return request({
        url: SECKILL_SKU_RELATION_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data,
    });
}



// 秒杀活动商品关联删除方法
export function removeSeckillSkuRelation(data) {
    return request({
        url: SECKILL_SKU_RELATION_URL + "/delete",
        method: 'post',
        data: data,
    });
}


// 秒杀活动商品关联启用方法
export function enableSeckillSkuRelation(data) {
    return request({
        url: SECKILL_SKU_RELATION_URL + "/enable",
        method: 'post',
        data: data,
    });
}


// 秒杀活动商品关联禁用方法
export function disableSeckillSkuRelation(data) {
    return request({
        url: SECKILL_SKU_RELATION_URL + "/disable",
        method: 'post',
        data: data,
    });
}








