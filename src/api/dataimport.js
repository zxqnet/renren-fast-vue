import request from '@/utils/httpRequest'


const BASE_URL = "/core-admin-service"


const URL = BASE_URL + "/web/v1.0/sys/data-import/template"



// 获取用户详细信息
export function pageList(data) {
    return request({
        url: URL + "/page/list",
        method: 'POST',
        data: data
    })
}

// 刪除
export function remove(data) {
    return request({
        url: URL + "/deletes",
        method: 'post',
        data: data
    });
}

export function enable(data) {
    return request({
        url: URL + "/enable",
        method: 'POST',
        data: data
    });
}

export function disable(data) {
    return request({
        url: URL + "/disable",
        method: 'POST',
        data: data
    });
}

export function saveAndUpdate(data) {
    return request({
        url: URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

export function findById(data) {
    return request({
        url: URL + "/findById",
        method: 'POST',
        data: data
    })
}
