import request from '@/utils/httpRequest'

const BASE_URL = "/small-coupon"

const COUPON_HISTORY_URL = BASE_URL + "/web/v1.0/coupon/couponhistory"



// 优惠券列表方法
export function pageCouponHistoryList(data) {
    return request({
        url: COUPON_HISTORY_URL + "/page/list",
        method: 'post',
        data: data,
    });
}

// 优惠券删除方法
export function couponHistoryRemove(data) {
    return request({
        url: COUPON_HISTORY_URL + "/delete",
        method: 'post',
        data: data,
    });
}