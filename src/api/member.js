import request from '@/utils/httpRequest'


const BASE_URL = "/small-member-service"


const MEMBER_URL = BASE_URL + "/web/v1.0/member/member"


const MEMBER_LEVEL_URL = BASE_URL + "/web/v1.0/member/level"
const MEMBER_COLLECT_SPU_URL = BASE_URL + "/web/v1.0/member/collect/spu"
const MEMBER_COLLECT_SUBJECT_URL = BASE_URL + "/web/v1.0/member/collect/subject"



// 会员列表信息
export function memberPageList(data) {
    return request({
        url: MEMBER_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 会员列表信息
export function memberRemove(data) {
    return request({
        url: MEMBER_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 会员等级列表分页信息
export function memberLevelPageList(data) {
    return request({
        url: MEMBER_LEVEL_URL + "/page/list",
        method: 'POST',
        data: data
    })
}

// 会员等级列表信息
export function memberLevelList() {
    return request({
        url: MEMBER_LEVEL_URL + "/data",
        method: 'get'
    })
}


// 会员等级保存与修改
export function memberLevelSaveAndUpdate(data) {
    return request({
        url: MEMBER_LEVEL_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}


// 会员等级删除
export function memberLevelRemove(data) {
    return request({
        url: MEMBER_LEVEL_URL + "/delete",
        method: 'POST',
        data: data
    })
}



// 商品收藏列表信息
export function collectSpuPageList(data) {
    return request({
        url: MEMBER_COLLECT_SPU_URL + "/page/list",
        method: 'POST',
        data: data
    })
}



// 商品收藏删除
export function collectSpuRemove(data) {
    return request({
        url: MEMBER_COLLECT_SPU_URL + "/delete",
        method: 'POST',
        data: data
    })
}



// 活动收藏列表信息
export function collectSubjectPageList(data) {
    return request({
        url: MEMBER_COLLECT_SUBJECT_URL + "/page/list",
        method: 'POST',
        data: data
    })
}



// 活动收藏删除
export function collectSubjectRemove(data) {
    return request({
        url: MEMBER_COLLECT_SUBJECT_URL + "/delete",
        method: 'POST',
        data: data
    })
}