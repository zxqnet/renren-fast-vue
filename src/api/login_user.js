import request from '@/utils/httpRequest'


const BASE_URL = "/core-admin-service"


const LOGIN_USER_URL = BASE_URL + "/web/v1.0/sys/user/login"

// 获取用户详细信息
export function updateLoginUserPassword(data) {
    return request({
        url: LOGIN_USER_URL + "/update-password",
        method: 'POST',
        data: data
    })
}
