import request from '@/utils/httpRequest'

const BASE_URL = "/fileupload"
const FILE_UPLOAD_URL = BASE_URL + "/web/v1.0/fileUpload/oss"

// "/upload-portal" 
// 上传文件
export const fileUpload = (data) => request({ url: FILE_UPLOAD_URL + '/upload', data: data, method: 'post' });

// 上传文件
export const getFileUploadDomain = () => request({ url: FILE_UPLOAD_URL + '/get-access-domain', method: 'get' });
