import request from '@/utils/httpRequest'
import qs from 'qs'


const BASE_URL = "/core-workflow-service"


const FLOW_URL = BASE_URL + "/web/v1.0/flow/definition"
const MODEL_URL = BASE_URL + "/web/v1.0/flow/model"
const TASK_URL = BASE_URL + "/web/v1.0/flow/task"
const RUN_URL = BASE_URL + "/web/v1.0/flow/run"
const FORM_MODEL_URL = BASE_URL + "/web/v1.0/flow/form/model"


// 获取列表信息详细信息
export function pageList(data) {
    return request({
        url: FLOW_URL + "/page/list",
        method: 'POST',
        data: data
    })
}

// 装换成模型操作
export function convertModel(data) {
    return request({
        url: FLOW_URL + "/page/list",
        method: 'POST',
        data: data
    })
}

// 挂起操作
export function hangUpEvent(data) {
    return request({
        url: FLOW_URL + "/updateState",
        method: 'POST',
        data: data
    })
}

// 流程管理删除
export function deleteEvent(data) {
    return request({
        url: FLOW_URL + "/delete",
        method: 'POST',
        data: data
    })
}



// 获取列表信息详细信息
export function pageModelList(data) {
    return request({
        url: MODEL_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 复制模型操作
export function copyModel(data) {
    return request({
        url: MODEL_URL + "/copy",
        method: 'POST',
        data: data
    })
}


// 获取列表信息详细信息
export function deploymentEvent(data) {
    return request({
        url: MODEL_URL + "/deployment",
        method: 'POST',
        data: data
    })
}

// 获取列表信息详细信息
export function pageRunList(data) {
    return request({
        url: RUN_URL + "/page/list",
        method: 'POST',
        data: data
    })
}



// 获取列表信息详细信息
export function pageDefinitionList(data) {
    return request({
        url: RUN_URL + "/page/list",
        method: 'POST',
        data: data
    })
}




export function readImgUrl(deploymentId) {
    return "/api" + BASE_URL + "/readXml?deployId=" + deploymentId
}



export function readXmlUrl(deploymentId) {
    return "/api" + BASE_URL + "/readImage?deployId=" + deploymentId
}




// 发起流程
export function startFlowPageList(data) {
    return request({
        url: TASK_URL + "/startFlow",
        method: 'POST',
        data: data
    })
}


// 我发起的流程
export function myProcessPageList(data) {
    return request({
        url: TASK_URL + "/myProcess",
        method: 'POST',
        data: data
    })
}

// 获取待办列表
export function todoPageList(data) {
    return request({
        url: TASK_URL + "/todoList",
        method: 'POST',
        data: data
    })
}

// 获取已办任务
export function finishedPageList(data) {
    return request({
        url: TASK_URL + "/finishedList",
        method: 'POST',
        data: data
    })
}



// 表单模型列表
export function formModelPageList(data) {
    return request({
        url: FORM_MODEL_URL + "/page/list",
        method: 'POST',
        data: data
    })
}

// 删除表单模型
export function formModelDeleteEvent(data) {
    return request({
        url: FORM_MODEL_URL + "/delete",
        method: 'POST',
        data: data
    })
}


// 删除表单模型
export function formModelSaveOrUpdate(data) {
    return request({
        url: FORM_MODEL_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

export function formModelEnableEvent(data) {
    return request({
        url: FORM_MODEL_URL + "/enable",
        method: 'POST',
        data: data
    })
}


export function formModelDisableEvent(data) {
    return request({
        url: FORM_MODEL_URL + "/disable",
        method: 'POST',
        data: data
    })
}





