import request from '@/utils/httpRequest'
import qs from 'qs'


const BASE_URL = "/core-admin-service"


const DB_URL = BASE_URL + "/web/v1.0/sys/database"
const TABLE_URL = BASE_URL + "/web/v1.0/sys/datatable"
const FIELD_URL = BASE_URL + "/web/v1.0/sys/field"
const FIELD_PROPERTY_URL = BASE_URL + "/web/v1.0/sys/field/property"


// 获取列表信息详细信息
export function pageList(data) {
    return request({
        url: DB_URL + "/page/list",
        method: 'POST',
        data: data
    })
}

export function saveOrUpdateDb(data){
    return request({
        url: DB_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

export function saveOrUpdateTable(data){
    return request({
        url: TABLE_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

export function saveOrUpdateField(data){
    return request({
        url: FIELD_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

export function removeDb(data){
    return request({
        url: DB_URL + "/delete",
        method: 'POST',
        data: data
    })
}

export function removeTable(data){
    return request({
        url: TABLE_URL + "/delete",
        method: 'POST',
        data: data
    })
}


export function removeField(data){
    return request({
        url: FIELD_URL + "/delete",
        method: 'POST',
        data: data
    })
}

export function enableDb(data){
    return request({
        url: DB_URL + "/enable",
        method: 'POST',
        data: data
    })
}

export function enableTable(data){
    return request({
        url: TABLE_URL + "/enable",
        method: 'POST',
        data: data
    })
}


export function enableField(data){
    return request({
        url: FIELD_URL + "/enable",
        method: 'POST',
        data: data
    })
}

export function disableDb(data){
    return request({
        url: DB_URL + "/disable",
        method: 'POST',
        data: data
    })
}

export function disableTable(data){
    return request({
        url: TABLE_URL + "/disable",
        method: 'POST',
        data: data
    })
}


export function disableField(data){
    return request({
        url: FIELD_URL + "/disable",
        method: 'POST',
        data: data
    })
}


export function pageFieldProperty(data){
    return request({
        url: FIELD_PROPERTY_URL + "/page/list",
        method: 'POST',
        data: data
    })
}

export function removeFieldProperty(data){
    return request({
        url: FIELD_PROPERTY_URL + "/delete",
        method: 'POST',
        data: data
    })
}


export function saveOrUpdateFieldProperty(data){
    return request({
        url: FIELD_PROPERTY_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}


