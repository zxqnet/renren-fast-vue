import request from '@/utils/httpRequest'


const BASE_URL = "/small-product-service"

const BRAND_URL = BASE_URL + "/web/v1.0/product/brand"
const CATEGORY_URL = BASE_URL + "/web/v1.0/product/category"
const PRODUCT_ATTR_URL = BASE_URL + "/web/v1.0/product/attr"
const PRODUCT_ATTR_GROUP_URL = BASE_URL + "/web/v1.0/product/attr-group"
const PRODUCT_SKUINFO_URL = BASE_URL + "/web/v1.0/product/sku-info"
const PRODUCT_SPUINFO_URL = BASE_URL + "/web/v1.0/product/spu-info"
const PRODUCT_CATEGORY_BRAND_RELATION_URL = BASE_URL + "/web/v1.0/product/category-brand"
const PRODUCT_ATTR_VALUE_URL = BASE_URL + "/web/v1.0/product/product-attr-value"
const PRODUCT_CATEGORY_ATTR_URL = BASE_URL + "/web/v1.0/product/category-attr"
const PRODUCT_ATTR_ATTR_GROUP_URL = BASE_URL + "/web/v1.0/product/attr-attr-group"
const PRODUCT_UNIT_URL = BASE_URL + "/web/v1.0/product/unit"
const PRODUCT_EXPRESS_URL = BASE_URL + "/web/v1.0/product/express"
const PRODUCT_MATERIAL_GROUP_URL = BASE_URL + "/web/v1.0/product/material-group"
const PRODUCT_MATERIAL_URL = BASE_URL + "/web/v1.0/product/material"




// 产品品牌请求列表
export function productBrandPageList(data) {
    return request({
        url: BRAND_URL + "/page/list",
        method: 'post',
        data: data
    })
}



// 产品品牌删除
export function removeProductBrand(data) {
    return request({
        url: BRAND_URL + "/delete",
        method: 'post',
        data: data
    })
}


export function productBrandList() {
    return request({
        url: BRAND_URL + "/data",
        method: 'get'
    })
}



// 产品品牌保存或者修改
export function addOrUpdateProductBrand(data) {
    return request({
        url: BRAND_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data
    })
}

// 产品品牌冻结
export function disableProductBrand(data) {
    return request({
        url: BRAND_URL + "/disable",
        method: 'post',
        data: data
    })
}

// 产品品牌启用
export function enableProductBrand(data) {
    return request({
        url: BRAND_URL + "/enable",
        method: 'post',
        data: data
    })
}

// 查询分类所对应的品牌
export function findProductBrandByCategoryId(data) {
    return request({
        url: PRODUCT_CATEGORY_BRAND_RELATION_URL + "/findBrandsByCategoryId",
        method: 'post',
        data: data
    })
}


// 查询分类所对应的品牌
export function addOrUpdateCategoryBrand(data) {
    return request({
        url: PRODUCT_CATEGORY_BRAND_RELATION_URL + "/update-category-brand",
        method: 'post',
        data: data
    })
}






// 产品分类请求列表
export function productCategoryTreeList() {
    return request({
        url: CATEGORY_URL + "/list/tree",
        method: 'get',
    })
}


// 产品分类请求列表
export function productCategoryPageList(data) {
    return request({
        url: CATEGORY_URL + "/list",
        method: 'post',
        data: data
    })
}

// 产品分类请求列表
export function productCategoryFindById(data) {
    return request({
        url: CATEGORY_URL + "/findById",
        method: 'post',
        data: data
    })
}





// 产品分类删除
export function removeProductCategory(data) {
    return request({
        url: CATEGORY_URL + "/delete",
        method: 'post',
        data: data
    })
}

// 产品分类保存或者修改
export function addOrUpdateProductCategory(data) {
    return request({
        url: CATEGORY_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data
    })
}



// 产品分类启用
export function enableProductCategory(data) {
    return request({
        url: CATEGORY_URL + "/enable",
        method: 'post',
        data: data
    })
}

// 产品分类冻结
export function disableProductCategory(data) {
    return request({
        url: CATEGORY_URL + "/disable",
        method: 'post',
        data: data
    })
}

// 产品分类管理查找分类路径
export function findAttrCategoryPath(data) {
    return request({
        url: CATEGORY_URL + "/getCategoryPath",
        method: 'post',
        data: data
    })
}



// 商品属性管理请求列表
export function productAttrPageList(data) {
    return request({
        url: PRODUCT_ATTR_URL + "/list",
        method: 'post',
        data: data
    })
}


// 商品属性管理请求列表
export function productAttrList(data) {
    return request({
        url: PRODUCT_ATTR_URL + "/data",
        method: 'post',
        data: data
    })
}




// 商品属性管理删除
export function removeProductAttr(data) {
    return request({
        url: PRODUCT_ATTR_URL + "/delete",
        method: 'post',
        data: data
    })
}



// 商品属性管理保存或者修改
export function addOrUpdateProductAttr(data) {
    return request({
        url: PRODUCT_ATTR_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data
    })
}


// 商品属性管理启用
export function enableProductAttr(data) {
    return request({
        url: PRODUCT_ATTR_URL + "/enable",
        method: 'post',
        data: data
    })
}

// 商品属性管理冻结
export function disableProductAttr(data) {
    return request({
        url: PRODUCT_ATTR_URL + "/disable",
        method: 'post',
        data: data
    })
}



// 商品属性管理冻结
export function getAttrByAttrGroupId(attrGroupId) {
    return request({
        url: PRODUCT_ATTR_URL + "/findAttrByAttrGroupId/" + attrGroupId,
        method: 'get',
    })
}










// 商品属性分组管理请求列表
export function productAttrGroupPageList(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/page/list",
        method: 'post',
        data: data
    })
}

export function productAttrGroupList(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/data",
        method: 'post',
        data: data
    })
}


export function getAttrGroupByAttrId(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/getAttrGroupByAttrId",
        method: 'post',
        data: data
    })
}




// 商品属性分组管理删除
export function removeProductAttrGroup(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/delete",
        method: 'post',
        data: data
    })
}

//  商品属性分组管理保存或者修改
export function addOrUpdateProductAttrGroup(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data
    })
}


//  商品属性分组管理保存或者修改
export function addOrUpdateAttrGroupAttr(data) {
    return request({
        url: PRODUCT_ATTR_ATTR_GROUP_URL + "/update-attr-group-attr",
        method: 'post',
        data: data
    })
}


//  商品属性分组分配商品分类
export function addOrUpdateProductAttrGroupCategory(data) {
    return request({
        url: PRODUCT_CATEGORY_ATTR_URL + "/update-attr-group-category",
        method: 'post',
        data: data
    })
}





// 产品分类启用
export function enableProductAttrGroup(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/enable",
        method: 'post',
        data: data
    })
}

// 产品分类冻结
export function disableProductAttrGroup(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/disable",
        method: 'post',
        data: data
    })
}







//  商品属性分组管理保存或者修改
export function findProductAttrGroupByCategoryId(categoryId) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/tree/" + categoryId,
        method: 'get'
    })
}




// 商品管理管理请求列表
export function productSkuInfoPageList(data) {
    return request({
        url: PRODUCT_SKUINFO_URL + "/page/list",
        method: 'post',
        data: data
    })
}






// 商品管理管理请求列表
export function productSpuInfoPageList(data) {
    return request({
        url: PRODUCT_SPUINFO_URL + "/page/list",
        method: 'post',
        data: data
    })
}



// 发布商品请求请求列表
export function productSpuBaseAttr(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/withBaseAttr",
        method: 'POST',
        data: data
    })
}

// 获取销售属性值
export function productSpuSaleAttr(data) {
    return request({
        url: PRODUCT_ATTR_GROUP_URL + "/withSaleAttr",
        method: 'POST',
        data: data
    })
}



// 发布商品请求请求列表
export function productSpuAttrValueInfoBySpuId(spuId) {
    return request({
        url: PRODUCT_ATTR_VALUE_URL + "/list/" + spuId,
        method: 'get',
    })
}





// 产品计量单位请求列表
export function productUnitPageList(data) {
    return request({
        url: PRODUCT_UNIT_URL + "/page/list",
        method: 'post',
        data: data
    })
}



// 产品计量单位删除
export function removeProductUnit(data) {
    return request({
        url: PRODUCT_UNIT_URL + "/delete",
        method: 'post',
        data: data
    })
}


// 计量单位列表
export function productBrandUnit() {
    return request({
        url: PRODUCT_UNIT_URL + "/data",
        method: 'get'
    })
}



// 产品计量单位保存或者修改
export function addOrUpdateProductUnit(data) {
    return request({
        url: PRODUCT_UNIT_URL + (!data.id ? "/save" : "/update"),
        method: 'post',
        data: data
    })
}

// 产品计量单位冻结
export function disableProductUnit(data) {
    return request({
        url: PRODUCT_UNIT_URL + "/disable",
        method: 'post',
        data: data
    })
}

// 产品计量单位启用
export function enableProductUnit(data) {
    return request({
        url: PRODUCT_UNIT_URL + "/enable",
        method: 'post',
        data: data
    })
}


// 快递公司 接口 Api

// 快递公司页面列表
export const getExpressPageList = (data) => request({ url: PRODUCT_EXPRESS_URL + '/page/list', data: data, method: 'post' });

// 快递公司删除
export const removeExpress = (data) => request({ url: PRODUCT_EXPRESS_URL + "/delete", method: 'post', data: data });

// 快递公司下拉列表
export const getExpressList = () => request({ url: PRODUCT_EXPRESS_URL + "/data", method: 'get' });

// 快递公司保存或者修改
export const addOrUpdateExpress = (data) => request({ url: PRODUCT_EXPRESS_URL + (!data.id ? "/save" : "/update"), method: 'post', data: data });

// 快递公司冻结
export const disableExpress = (data) => request({ url: PRODUCT_EXPRESS_URL + "/disable", method: 'post', data: data });

// 快递公司启用
export const enableExpress = (data) => request({ url: PRODUCT_EXPRESS_URL + "/enable", method: 'post', data: data });


// 素材分组 接口 Api

// 素材分组页面列表
export const getMaterialGroupPageList = (data) => request({ url: PRODUCT_MATERIAL_GROUP_URL + '/page/list', data: data, method: 'post' });

// 素材分组删除
export const removeMaterialGroup = (data) => request({ url: PRODUCT_MATERIAL_GROUP_URL + "/delete", method: 'post', data: data });

// 素材分组下拉列表
export const getMaterialGroupList = () => request({ url: PRODUCT_MATERIAL_GROUP_URL + "/data", method: 'get' });

// 素材分组保存或者修改
export const addOrUpdateMaterialGroup = (data) => request({ url: PRODUCT_MATERIAL_GROUP_URL + (!data.id ? "/save" : "/update"), method: 'post', data: data });

// 素材分组查询数据
export const getMaterialGroupById = (data) => request({ url: PRODUCT_MATERIAL_GROUP_URL + "/findById", method: 'post', data: data });

// 素材分组冻结
export const disableMaterialGroup = (data) => request({ url: PRODUCT_MATERIAL_GROUP_URL + "/disable", method: 'post', data: data });

// 素材分组启用
export const enableMaterialGroup = (data) => request({ url: PRODUCT_MATERIAL_GROUP_URL + "/enable", method: 'post', data: data });


// 素材库页面列表
export const getMaterialPageList = (data) => request({ url: PRODUCT_MATERIAL_URL + '/page/list', data: data, method: 'post' });

// 素材库删除
export const removeMaterial = (data) => request({ url: PRODUCT_MATERIAL_URL + "/delete", method: 'post', data: data });

// 素材库下拉列表
export const getMaterialList = () => request({ url: PRODUCT_MATERIAL_URL + "/data", method: 'get' });

// 素材库保存或者修改
export const addOrUpdateMaterial = (data) => request({ url: PRODUCT_MATERIAL_URL + (!data.id ? "/save" : "/update"), method: 'post', data: data });

// 素材库查询数据
export const getMaterialById = (data) => request({ url: PRODUCT_MATERIAL_URL + "/findById", method: 'post', data: data });

// 素材库冻结
export const disableMaterial = (data) => request({ url: PRODUCT_MATERIAL_URL + "/disable", method: 'post', data: data });

// 素材库启用
export const enableMaterial = (data) => request({ url: PRODUCT_MATERIAL_URL + "/enable", method: 'post', data: data });

// 素材库上传文件
export const batchUpdateMaterial = (data) => request({ url: PRODUCT_MATERIAL_URL +  "/fileUpdate", method: 'post', data: data });
