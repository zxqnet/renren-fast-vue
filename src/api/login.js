import request from '@/utils/httpRequest'
import qs from 'qs'


const BASE_URL = "/core-auth-service"

// 登录
export const login = (data) => request({
     url: BASE_URL +"/oauth/token", 
     method: "post", 
     params:qs.stringify(data),
     headers: {
        "Content-Type":
            "application/x-www-form-urlencoded; charset=utf-8"
        },
    })

// 退出方法
export const logout = (data) =>  request({ url: BASE_URL + "/oauth/remove/token", method: 'post',data: data})

// 获取验证码
export const getCodeImg = (data) => request({ url: BASE_URL + "/oauth/validation/code/" + data + "?_=" + Date.now(), method: 'get'})

