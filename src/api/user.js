import request from '@/utils/httpRequest'

// URl 地址
const BASE_URL = "/core-admin-service"
const USER_URL = BASE_URL + "/web/v1.0/sys/user"


// 获取当前登录用户详细信息
export const getUserInfo = (data) => request({ url: USER_URL +"/login/userInfo", method: "get", data:data })

// 获取用户列表信息
export const pageList  = (data) => request({ url: USER_URL + "/page/list",  method: 'POST', data: data })

// 刪除用户
export const remove = (data) => request({ url: USER_URL + "/deletes",  method: 'post', data: data })

//启用
export const enable = (data) => request({ url: USER_URL + "/enable",  method: 'POST', data: data })

// 禁用、冻结
export const disable = (data) => request({ url: USER_URL + "/disable",   method: 'POST', data: data })

// 重置密码
export const resetPassword = (data) => request({url: USER_URL + "/resetPassword",method: 'POST', data: data  })

// 保存与修改
export const saveAndUpdate = (data) => request({ url: USER_URL + (!data.id ? "/save" : "/update"), method: 'POST',data: data })

// 通过ID查找
export const findById = (data)  => request({url: USER_URL + "/findById",method: 'POST', data: data })


// 导出
export const exportExcel = (data)  => request({url: USER_URL + "/export",method: 'POST', data: data })
