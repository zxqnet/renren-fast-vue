import request from '@/utils/httpRequest'


const BASE_URL = "/core-admin-service"
const DICT_URL = BASE_URL + "/web/v1.0/sys/dict"


// 查询列表
export const pageList = (data) => request({url: DICT_URL + "/list",   method: 'post', data: data })

// 删除
export const remove =(data) => request({ url: DICT_URL + "/deletes", method: 'post',data: data})

//启用
export const enable = (data) => request({ url: DICT_URL + "/enable",  method: 'POST', data: data })

// 禁用、冻结
export const disable = (data) => request({ url: DICT_URL + "/disable",   method: 'POST', data: data })


// 保存与删除
export const saveOrUpdate = (data) => request({url: DICT_URL + (!data.id ? "/save" : "/update"),  method: 'post',  data: data})
