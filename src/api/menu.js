import request from '@/utils/httpRequest'
import qs from 'qs'


const BASE_URL = "/core-admin-service"

const MENU_URL = BASE_URL + "/web/v1.0/sys/menus"


// 退出方法
export function pageList(data) {
    return request({
        url: MENU_URL + "/page/list",
        method: 'post',
        data: data
    })
}

export function getMenuTop() {
    return request({
        url: MENU_URL + "/findTop",
        method: 'get',
    })
}

export function saveAndUpdate(data) {
    return request({
        url: MENU_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data:data
    })
}

export function getInfo(data) {
    return request({
        url: MENU_URL + "/findById",
        method: 'POST',
        data:data
    })
}

export function remove(data) {
    return request({
        url: MENU_URL + "/delete",
        method: 'POST',
        data:data
    })
}


