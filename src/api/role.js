import request from '@/utils/httpRequest'


const BASE_URL = "/core-admin-service"
const ROLE_URL = BASE_URL + "/web/v1.0/sys/role"



// 查询列表
export const pageList = (data) => request({ url: ROLE_URL + "/page/list", method: 'post', data: data })

// 查询下拉
export const roleList = (data) => request({ url: ROLE_URL + "/list", method: 'post', data: data })

//保存和编辑
export const saveAndUpdate = (data) => request({ url: ROLE_URL + (!data.id ? "/save" : "/update"), method: 'POST', data: data })


// 刪除
export const remove = (data) => request({ url: ROLE_URL + "/deletes", method: 'post', data: data });

// 启用
export const enable = (data) => request({ url: ROLE_URL + "/enable", method: 'POST', data: data })

//禁用、冻结
export const disable = (data) => request({
    url: ROLE_URL + "/disable", method: 'POST', data: data
})

// 查询菜单权限
export const findByMenuIds = (data) => request({
    url: ROLE_URL + "/findByMenuIds", method: 'POST', data: data
})



