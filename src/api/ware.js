import request from '@/utils/httpRequest'

const BASE_URL = "/small-ware-service"

const WARE_URL = BASE_URL + "/web/v1.0/ware/ware/info"
const WARE_ORDER_TASK_URL = BASE_URL + "/web/v1.0/ware/ware/order/task"
const WARE_SKU_URL = BASE_URL + "/web/v1.0/ware/ware/sku"
const WARE_PURCHASE_URL = BASE_URL + "/web/v1.0/ware/purchase"
const WARE_PURCHASE_DEATAIL_URL = BASE_URL + "/web/v1.0/ware/purchase/detail"
const WARE_ORDER_TASK_DETAIL_URL = BASE_URL + "/web/v1.0/ware/ware/order/task/detail"

// 仓库列表
export function wareInfopageList(data) {
    return request({
        url: WARE_URL + "/page/list",
        method: 'POST',
        data: data
    })
}



// 保存仓库
export function wareInfoList() {
    return request({
        url: WARE_URL + "/list",
        method: 'get'
    })
}


// 保存仓库
export function wareInfoSaveAndUpdate(data) {
    return request({
        url: WARE_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}


// 保存仓库
export function wareInfoRemove(data) {
    return request({
        url: WARE_URL + "/delete",
        method: 'POST',
        data: data
    })
}


// 保存仓库
export function wareInfoDisable(data) {
    return request({
        url: WARE_URL + "/disable",
        method: 'POST',
        data: data
    })
}

export function wareInfoEnable(data) {
    return request({
        url: WARE_URL + "/enable",
        method: 'POST',
        data: data
    })
}




// 库存工作单列表
export function wareOrderTaskpageList(data) {
    return request({
        url: WARE_ORDER_TASK_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 库存工作单
export function wareOrderTaskSaveAndUpdate(data) {
    return request({
        url: WARE_ORDER_TASK_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

// 删除工作单
export function wareOrderTaskRemove(data) {
    return request({
        url: WARE_ORDER_TASK_URL + "/delete",
        method: 'POST',
        data: data
    })
}




// 商品库存列表
export function wareSkuPageList(data) {
    return request({
        url: WARE_SKU_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 商品库存保存与新增
export function wareSkuSaveAndUpdate(data) {
    return request({
        url: WARE_SKU_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

// 商品库存删除
export function wareSkuRemove(data) {
    return request({
        url: WARE_SKU_URL + "/delete",
        method: 'POST',
        data: data
    })
}





// 采购信息分页列表
export function warePurchasePageList(data) {
    return request({
        url: WARE_PURCHASE_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 采购信息列表
export function warePurchaseList() {
    return request({
        url: WARE_PURCHASE_URL + "/data",
        method: 'get'
    })
}


// 采购信息保存与新增
export function warePurchaseSaveAndUpdate(data) {
    return request({
        url: WARE_PURCHASE_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

// 采购信息删除
export function warePurchaseRemove(data) {
    return request({
        url: WARE_PURCHASE_URL + "/delete",
        method: 'POST',
        data: data
    })
}



// 采购需求信息列表
export function warePurchaseDeatailPageList(data) {
    return request({
        url: WARE_PURCHASE_DEATAIL_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 采购需求保存与新增
export function warePurchaseDeatailSaveAndUpdate(data) {
    return request({
        url: WARE_PURCHASE_DEATAIL_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

// 采购需求删除
export function warePurchaseDeatailRemove(data) {
    return request({
        url: WARE_PURCHASE_DEATAIL_URL + "/delete",
        method: 'POST',
        data: data
    })
}


// 采购需求合并单请求数据
export function warePurchaseUnreceive() {
    return request({
        url: WARE_PURCHASE_URL + "/unreceive/list",
        method: 'get'
    })
}

// 采购需求合并单
export function warePurchaseMerge(data) {
    return request({
        url: WARE_PURCHASE_URL + "/merge",
        method: 'POST',
        data:data
    })
}




// 库存工作单详情信息列表
export function wareOrderTaskDeatailPageList(data) {
    return request({
        url: WARE_ORDER_TASK_DETAIL_URL + "/page/list",
        method: 'POST',
        data: data
    })
}


// 库存工作单详情保存与新增
export function wareOrderTaskDeatailSaveAndUpdate(data) {
    return request({
        url: WARE_ORDER_TASK_DETAIL_URL + (!data.id ? "/save" : "/update"),
        method: 'POST',
        data: data
    })
}

// 库存工作单详情删除
export function wareOrderTaskDeatailRemove(data) {
    return request({
        url: WARE_ORDER_TASK_DETAIL_URL + "/delete",
        method: 'POST',
        data: data
    })
}
