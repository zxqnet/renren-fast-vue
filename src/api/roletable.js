import request from '@/utils/httpRequest'
import qs from 'qs'


const BASE_URL = "/core-admin-service"


const DB_URL = BASE_URL + "/web/v1.0/sys/database"
const TABLE_URL = BASE_URL + "/web/v1.0/sys/datatable"
const FIELD_URL = BASE_URL + "/web/v1.0/sys/field"



export function addRoleDb(data){
    return request({
        url: DB_URL + "/addRole",
        method: 'POST',
        data: data
    })
}

export function addRoleTable(data){
    return request({
        url: TABLE_URL + "/addRole",
        method: 'POST',
        data: data
    })
}


export function addRoleField(data){
    return request({
        url: FIELD_URL + "/addRole",
        method: 'POST',
        data: data
    })
}