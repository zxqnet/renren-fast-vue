export default {
  namespaced: true,
  state: {
    // 用户ID
    id: 0,
    // 用户登录名
    username:'',
    //用户姓名
    name: ''
  },
  mutations: {
    updateId (state, id) {
      state.id = id
    },
    updateName (state, name) {
      state.name = name
    },
    updateUserName (state, username) {
      state.username = username
    }
  }
}
