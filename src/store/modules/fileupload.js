import { getFileUploadDomain } from "@/api/fileUpload";
//文件上传模块的仓库
const state = {
  //文件上传的前缀 路径
  fileDomain: "",
};

//mutions是唯一修改state的地方
const mutations = {
  GETFILEDOMAIN(state, fileDomain) {
    state.fileDomain = fileDomain;
  },
};

//action|用户处理派发action地方的，可以书写异步语句、自己逻辑地方
const actions = {

  async getFileDomain({ commit }) {
    //reqgetCategoryList返回的是一个Promise对象
    //需要用await接受成功返回的结果，await必须要结合async一起使用（CP）
    let result = await getFileUploadDomain();
    if (result && result.code == 0) {
      commit("GETFILEDOMAIN", result.data);
    }
  },
};

//计算属性
const getters = {};

export default {
  state,
  mutations,
  actions,
  getters,
};
